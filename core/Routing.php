<?php

class Routing
{

	/* Test ID: 1710434661 */
	public static function route(){
		
		if ( empty($_GET['url']) ){
			$_GET['url']="/test/testing";
		}

		$url=trim($_GET['url'],'/');
		$requestParts= explode('/',$url);
		if( empty($requestParts[0]) ){ $requestParts[0]=null; }
		if( empty($requestParts[1]) ){ $requestParts[1]=null; }

		define('CONTROLLER_NAME', $requestParts[0]);
		define('METHOD_NAME', $requestParts[1]);

		$class=CONTROLLER_NAME . "Controller";
		if ( class_exists($class) ){
			$controller= new $class();
			if( !empty( METHOD_NAME ) and method_exists( $class , METHOD_NAME ) ){

				$response = $controller->{METHOD_NAME}();

				if ( empty($response['type']) ){
					return "1714748530 - This endpoint has not set a response type";
				}
				elseif($response['type']==="json"){
					header("Content-type:application/json");
					echo json_encode($response['response']);
					return $response;
				}
				elseif( $response['type']==="view" ){
					Tools::renderView( $response['viewLocation'], $response['data'] );
					return;
				}
				elseif( $response['type']==="resource" ){
					return true;
				}
				else{
					return "1714748531 - This endpoint response type is unknown.";
				}				
			}
			else{
				http_response_code(404);
				return "1714748533 - The requested method does not exist.";
			}
		}
		else{
			http_response_code(404);
			header("Content-type:application/json");
			$response= [
				"status"=> false,
				"error"=> "404 - Not found. The requested endpoit does not exist in this API."
			];
			echo json_encode($response);
			return $response;
		}
	}

}



