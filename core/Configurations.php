<?php

class Configurations
{

	public static function setEnvParams(){
		$env = file_get_contents(__DIR__."/../.env");
		$lines = explode("\n",$env);
		
		foreach($lines as $line){
			//Ignore lines starting with "#" or don't have a value after "="
			preg_match("/([^#]+)\=(.*)/", $line, $matches);

			if(isset($matches[2])){
				$_ENV[$matches[1]]=trim($matches[2]);
				putenv(trim($line));
			}
		} 
	}

	public static function setGlobalConfigurations(){
		/*
		Windows hosts dont provide some $_SERVER variables. We make sure they exist by setting them with PHP functions.
		Comment this if you have no use for these $_SERVER variables.  */
		if ( empty($_SERVER["SERVER_NAME"]) ){
			$_SERVER["SERVER_NAME"]= gethostname();
		}
		if ( empty($_SERVER["SERVER_ADDR"]) ){
			$_SERVER["SERVER_ADDR"] = gethostbyname( $_SERVER["SERVER_NAME"] );
		}

		/* 
		Comment this line if your application is allowed to run without SSL */
		//Tools::httpsIsMandatory();

		if ( $_ENV["ENVIRONMENT"] === "production" ){
			error_reporting(0);
			ini_set("display_errors",0);	
		}
		else{
			error_reporting(E_ALL);
			ini_set("display_errors",1);	
		}

		define ("__PUBLIC_PATH__", 			__DIR__."/../public/");
		define ("__VIEWS_PATH__", 			__DIR__."/../app/views/");
		define ("__MODELS_PATH__", 			__DIR__."/../app/models/");
		define ("__CONTROLLERS_PATH__", 	__DIR__."/../app/controllers/");
	}

}

?>