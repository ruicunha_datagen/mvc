<?php


class Logging
{
	public $textLogsDirectory = __DIR__ . "/textLogs/"; /* The directory on your filesystem where fallback text logs are written*/
	public $connection;	/* PDO Object with write permissions. Use it when instatiating the class. */
	public $dateTime;
	public $clientId;	
	public $userId;
	public $levelId;	/* Links to the table logs_levels */
	public $groupId;	/* Links to the table logs_groups */
	public $title;		/* short text */
	public $description;	/* long text */
	public $json;		/* store here whatever data you need */
	public $sessionId;
	public $ip;
	public $userAgent;
	public $controller;	/* to use in MVC structures */
	public $method;	/* to use in MVC structures */
	public $writeLocation;	/* a unique ID that identifies the exact place on your code that wrote each log */
	private $duration;	/* microseconds that your script ran before writting the log. Use it for performance analytics and to identify bottlenecks	*/
	
	public function __construct( PDO $dbConnection ){
		$this->connection = $dbConnection;
	}
	
	public function write( $fallbackToDisk=true  ){
		
		$this->clientId = 	is_int($this->clientId) ? $this->clientId : "null";
		$this->userId = 	is_int($this->userId) ? $this->userId : "null";
		$this->levelId = 	is_int($this->levelId) ? $this->levelId : "null";
		$this->groupId = 	is_int($this->groupId) ? $this->groupId : "null";
		$this->dateTime =	empty($this->dateTime) ? 
								intval($_SERVER['REQUEST_TIME_FLOAT']):
								$this->dateTime;
		$this->userAgent =	empty($this->userAgent) ? $_SERVER['HTTP_USER_AGENT'] : $this->userAgent;
		$this->duration =	empty($this->duration) ?
								( microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'] ) * 1000000 :
								$this->duration;
		$this->ip =			empty($this->ip) ? 
								(php_sapi_name() === 'cli' || empty($_SERVER['REMOTE_ADDR']) ? 'cli' : $_SERVER['REMOTE_ADDR']) :
								$this->ip;
		$this->sessionId =  empty($this->sessionId) ? session_id() : $this->sessionId;
		
		if( $this->connection instanceof PDO ){
			try{
				$queryString="
					insert into logs
						(	clientId,
							userId,
							dateTime,
							levelId,
							groupId,
							title,
							description,
							sessionId,
							ip,
							userAgent,
							json,
							controller,
							method,
							microsecondsSpent,
							writeLocation
							)
					values
						(	" . $this->clientId . ",
							" . $this->userId . ",
							'" . date('Y-m-d H:i:s', $this->dateTime ) . "',
							" . $this->levelId . ",
							" . $this->groupId . ",
							:_title,
							:_description,
							:_sessionId,
							:_ip,
							:_userAgent ,
							:_json,
							:_controllerName,
							:_methodName,
							" . $this->duration . ",
							:_writeLocation
							)
				";
				$query = $this->connection -> prepare($queryString);
				$query->bindParam(':_title',			$this->title , PDO::PARAM_STR);
				$query->bindParam(':_description',		$this->description , PDO::PARAM_STR);
				$query->bindParam(':_sessionId',		$this->sessionId , PDO::PARAM_STR);
				$query->bindParam(':_userAgent', 		$this->userAgent , PDO::PARAM_STR);
				$query->bindParam(':_json', 			$this->json , PDO::PARAM_STR);
				$query->bindParam(':_controllerName', 	$this->controller , PDO::PARAM_STR);
				$query->bindParam(':_methodName', 		$this->method , PDO::PARAM_STR);
				$query->bindParam(':_writeLocation', 	$this->writeLocation , PDO::PARAM_STR);
				$query->bindParam(':_ip', 				$this->ip , PDO::PARAM_STR);
				$query -> execute();
				
				$this->resetLogData();

				return array(
					"status"=>true,
					"message"=>'Log was successfully written'
				);
			}
			catch(PDOException $e){
				if( $fallbackToDisk ){
					$this->writeLogToTextFile();
				}
			}
			$this->resetLogData();
			return array(
				"status"=>false,
				"message"=>"Could not write log to database.",
				"errorLocation"=>1715202023,
				"errorMessage"=>$e->getMessage()
			);
		}
		
		if( $fallbackToDisk ){
			$this->writeLogToTextFile();
		}
		$this->resetLogData();
		return array(
			"status"=>false,
			"message"=>"No valid DB connection provided.",
			"errorLocation"=>1715202024
		);	

	}
	
	public function resetLogData(){
		/* cleaning up the object */
		$this->clientId=null;
		$this->userId=null;
		$this->dateTime=null;
		$this->levelId=null;
		$this->groupId=null;
		$this->title=null;
		$this->description=null;
		$this->sessionId=null;
		$this->ip=null;
		$this->userAgent=null;
		$this->json=null;
		$this->controller=null;
		$this->method=null;
		$this->duration=null;
		$this->writeLocation=null;
	}
	
	public function writeLogToTextFile(){
		/*
		This function makes sure that logs are not lost when you cannot write on the database.
		It's a fallback mechanism, eventually you will need to collect them from the disk to the database.
		Use the method importLogsFromTextFilesToDatabase() to do it */
		
		if( !is_dir( $this->textLogsDirectory )) {
			mkdir( $this->textLogsDirectory );
		}
		
		// selects only the public properties
		$vars=call_user_func('get_object_vars', $this);
		//unsets the ones that are not needed
		unset( $vars['textLogsDirectory'] );
		unset( $vars['connection'] );
		
		$data=json_encode( $vars );
		try{
			file_put_contents( $this->textLogsDirectory . microtime(true) . ".log.txt", $data );
			return array(
				"status"=>true,
				"message"=>"A log text file has been written in " . $this->textLogsDirectory . "."
			);
		}catch(exception $e){
			return array(
				"status"=>false,
				"message"=>'A log could not be written to the filesystem. The server response was: ' . chr(10) . $e->getMessage()
			);
		}
	}

	public function importLogsFromTextFilesToDatabase(){
		/* from time to time, you need to collect text files that were placed in your filesystem. */
		
		foreach (new DirectoryIterator( $this->textLogsDirectory ) as $file) {
			if( $file->isDot() ) continue;
			$content=file_get_contents( $file->getPathname() );
			$data=(array) json_decode( $content, true );
			foreach( $data as $key=>$value ){
				$this->{$key} = $value;
			}
			$result=$this->write( $fallbackToDisk=false );
			if( $result['status']===true ){
				unlink( $file->getPathname() );
			}
		}
	}
	
	public function createDatabaseStructure(){
		// Use this method to create the database structure required for the class to work
		
		try{		
			$queryString="
				CREATE DATABASE IF NOT EXISTS `logs`;
				
				USE `logs`;
				
				CREATE TABLE IF NOT EXISTS `logs` (
					`clientId` INT(10) UNSIGNED NULL DEFAULT NULL,
					`userId` INT(10) UNSIGNED NULL DEFAULT NULL,
					`dateTime` TIMESTAMP NULL DEFAULT NULL,
					`levelId` INT(10) UNSIGNED NULL DEFAULT NULL,
					`groupId` INT(10) UNSIGNED NULL DEFAULT NULL,
					`title` TEXT(65535) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
					`description` TEXT(65535) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
					`sessionId` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
					`ip` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
					`userAgent` VARCHAR(100) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
					`json` TEXT(65535) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
					`controller` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
					`method` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci',
					`microsecondsSpent` BIGINT(15) UNSIGNED NULL DEFAULT NULL,
					`writeLocation` VARCHAR(50) NULL DEFAULT NULL COLLATE 'utf8mb4_general_ci'
				)
				COLLATE='utf8mb4_general_ci'
				ENGINE=InnoDB;
				
				CREATE TABLE IF NOT EXISTS `logs_levels` (
					`id` SMALLINT(3) UNSIGNED NOT NULL,
					`level` VARCHAR(50) NOT NULL COLLATE 'utf8_general_ci',
					PRIMARY KEY (`id`) USING BTREE
				)
				COLLATE='utf8_general_ci'
				ENGINE=InnoDB;
				
				INSERT INTO `logs_levels` (`id`, `level`) VALUES
					(100, 'Information'),
					(200, 'Debug'),
					(300, 'Alert'),
					(400, 'Critical');
					
				CREATE TABLE IF NOT EXISTS `logs_groups` (
				  `id` smallint(6) unsigned NOT NULL,
				  `group` varchar(50) NOT NULL,
				  PRIMARY KEY (`id`)
				) 
				COLLATE='utf8_general_ci'
				ENGINE=InnoDB;
				
				INSERT INTO `logs_groups` (`id`, `group`) VALUES
					(100, 'Generic'),
					(200, 'Performance'),
					(300, 'Database'),
					(400, 'Security');
					";
			$this->connection -> exec($queryString);
			
			return array(
				"status"=>true,
				"message"=>'database was successfully created'
			);
			
		}catch(PDOException $e){

			echo $e->getMessage();
		}

	}

}

?>