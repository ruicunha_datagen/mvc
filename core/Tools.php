<?php

class Tools
{

	// requesting a database connection
	// SQL injection starts with the terrible habit of using a full privileged connection for all queries in an application.
	// Most queries will only require READ privilege, so keep a separate connection with that permission alone.
	// If you need to UPDATE or INSERT, do not use a connection that also has DROP privilege. Keep a separate one for that also.
	// In any case, GRANT ALL PRIVILEGES is your enemy.
	public static function getDatabaseConnection( $type='RO' ){		
		if( $type==='RW' ){
			$username=$_ENV["MARIADB_RW_USERNAME"];
			$password=$_ENV["MARIADB_RW_PASSWORD"];
		}else{
			$username=$_ENV["MARIADB_RO_USERNAME"];
			$password=$_ENV["MARIADB_RO_PASSWORD"];
		}
		
		try {
			$connection = new PDO(
					"mysql:	host=" . $_ENV["MARIADB_HOST"] . ";
							dbname=" . $_ENV["MARIADB_NAME"] . ";
							charset=utf8",
					$username ,
					$password ,
					array(	PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
							PDO::ATTR_EMULATE_PREPARES => false)
			);
			return $connection;	
		}
		catch (PDOException $e) {
			die('Database connection failed.');
		}
	}

	public static function isValidEmail( $email = '', $checkMxRecords=true){
		try{
			// Remove all illegal characters from email
			$email = filter_var($email, FILTER_SANITIZE_EMAIL);
			// Validate e-mail
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				throw new Exception('O email é inválido');
			}
			// check mx records
			list($username, $domain) = explode('@', $email);
			if ( !checkdnsrr($domain, 'MX')) {
				throw new Exception('O email é inválido.');
			}
			return array(
				"status"=>true,
				"message"=>"O email é válido."
			);
		}catch( Exception $e){
			return array(
				"status"=>false,
				"message"=>$e->getMessage()
			);
		}
	}

	public static function isValidDate($date, $format='Y-m-d'){
			
		if(substr($date,0,10)==='0000-00-00' || $date===null || $date===""){
			return false;
		}

		$d = DateTime::createFromFormat($format, $date);
		
		if ( !($d && $d->format($format) === $date) ){
			return false;
		}else{
			return array(
				'status'=>true,
				'mysql_format'=>$d->format('Y-m-d H:i:s'),
				'pt_format'=>$d->format('d-m-Y H:i:s')
				);
		}
	}

	public static function renderView( $viewPath, $data ){
		extract($data);
		include $viewPath;
	}

	public static function httpsIsMandatory(){
		
		if(  (! empty($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'https') ||
			(! empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ||
			(! empty($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] == '443') ||
			(! empty($_SERVER['HTTP_CF_VISITOR']) && $_SERVER['HTTP_CF_VISITOR'] == '{"scheme":"https"}')
			){
			return true;
		}else{
			header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"], true, 301);
			die();
		}
	}
	
}






	



