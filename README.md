###### Author: Rui Cunha
###### Email: r.cunha@datagen.eu

### About this project
Modern frameworks are a pain, a technical overkill, and the reason why so many programmers are completely oblivious about how stuff really works.
This project is the base structure upon which I build all my other projects. It is an implementantion of the MVC pattern used in all modern PHP frameworks, and it's basically a set of five very small files working together to provide you a framed flow for your code. Everything else is composer stuff.

###### How to set it up
1. Copy this project to your server.
2. Make the folder 'public' your web root.
3. Change to file /core/Configurations.php with your own settings.

###### How it works
Lets go through all the five files:

### The /public/.html file
This is the first file met by incoming requests. It will rewrite the request URL and redirect it to the index.php file in the following pattern:
Original URL:	https://www.mybelovedserver/home/123
Becomes: 		https://www.mybelovedserver/index.php?url=home/123
Notice that now you have a $_GET variable holding the final part of the original address.
All incoming requests that do not meet an existing file on your web folder will now fallback to your index.php file, making it the entry point of your application.

### The /public/index.php file
This is the entry point of your application. It does only two things:
1. Includes the /core/Configurations.php file, which sets the predefined settings your program will require.
2. Runs the routing file /core/Routing.php.

### The /core/Tools.php file
Just a collection of tools used throughout the application