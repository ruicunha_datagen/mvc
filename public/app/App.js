var App = {

    /** Determines hat happens when an Ajax call is started */
    whenAjaxStart: ()=>{
        $(document).ajaxStart(function () {
            Tools.displaySpinner(true);
        })
    },

    /** Determines hat happens when an Ajax call, or the last of simultaneous ones ends */
    whenAjaxFinish: ()=>{
        $(document).ajaxComplete(function(event, request){
            Tools.displaySpinner(false);
            try {
                var response=JSON.parse(request.responseText);
                if(response.errorCode===401){
                    
                }
            } catch (e) {
                return;
            }
        })
    },

    loadTemplate: function (template, container, callback = null) {
        $.get(template, function(data, status){
            document.getElementById(container).innerHTML = data.response
            if (typeof (callback) === "function") {
                callback();
            }
        });
    },
    
    displayModal: function(){
        $("#myModal").html(`
            <div class="modal-dialog">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Modal body text goes here.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
                </div>
            </div>
        `);
    },

    route: function(){
        var url = window.location.hash.slice(1);
        if(!url){
            return;
        }
        console.log(url)
        
        //sometimes GET params are appended at the end of url string.
        //only the first part is used for routing
        var parts=url.split('?');

        $.getScript( parts[0] + "/route.js").fail(function() {
            $("#myModal").modal('show')
            $("#myModal .modal-title").html("404 - Page not found")
            $("#myModal .modal-body").html("The requested url does not exist.")
        });
    },

    // This function allows you to store arrays/objects as cookies. 
    // Use Tools.getCookie to retrieve them in the original format
    setCookie: function(name, value, validityDays=30) {
        if (typeof(value)==='object'){
            value=JSON.stringify(value);
        }
        let date = new Date();
        date.setTime(date.getTime() + (validityDays * 24 * 60 * 60 * 1000));
        const expires = "expires=" + date.toUTCString();
        document.cookie = name + "=" + value + "; " + expires + "; path=/; SameSite=Lax";
    },
    
    getCookie: function(cookieName) {
        const name = cookieName + "=";
        const cDecoded = decodeURIComponent(document.cookie); //to be careful
        const cArr = cDecoded .split('; ');
        let res=false;
        cArr.forEach(val => {
            if (val.indexOf(name) === 0){
                res = val.substring(name.length);
                if ( Tools.isJson(res) ){
                    res=JSON.parse(res);
                }
            }
        })
        return res;
    },

    signalError: function( jQueryObject ){
        jQueryObject.css("background-color", "#f2dada");
        setTimeout(function(){
            jQueryObject.css("background-color", "transparent");
        }, 3000)
    },

}