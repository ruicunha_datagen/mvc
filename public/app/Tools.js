/**
 * Set of the generic functions and params used by the application
 */

var Tools = {


    scrollToTop: function () {
        $('html, body').animate({ scrollTop: 0 }, 200);
    },

    displaySpinner: function (status = true) {
        if (status === true) {
            if (!$("#spinnerLayer").length) {
                var layer = document.createElement("div");
                layer.id = "spinnerLayer";
                document.body.appendChild(layer);
                layer.style.zIndex = '5000';
                layer.style.height = "100%";
                layer.style.width = "100%";
                layer.style.position = 'fixed';
                layer.style.top = "0";
                layer.style.left = "0";
                layer.style.backgroundColor = "white";
                layer.style.opacity = "0.8";
            } else {
                $("#spinnerLayer").show();
            }

            if (!$("#spinnerGif").length) {
                var spinner = document.createElement("img");
                spinner.id = "spinnerGif";
                spinner.src = "/images/loading.gif";
                document.body.appendChild(spinner);
                spinner.style.zIndex = '5001';
                spinner.style.position = "fixed";
                spinner.style.top = "50vh";
                spinner.style.marginLeft = "50vw";
                spinner.style.width = "50px";
            } else {
                $("#spinnerGif").show()
            }

        } else {
            $("#spinnerLayer").hide();
            $("#spinnerGif").hide();
        }
    },

    dateManagement: {

        /**
        * acepted input formats for data:	(empty) -> returns now
        *                                 MM/DD/YYYY
        *                                 YYYY-MM-DD
        *                                 MMM DD YYYY (ex: Mar 03 2015, March 03 2015)
        *                                 DD MMM YYYY (ex: 03 Mar 2015)
        *                                 YYYY-MM-DDTHH:MM:SSZ (ex:2015-03-25T12:00:00Z)
        * Can also be used to check if any date is actually valid. 
        * 2018-02-29 will return false, but 2020-02-29 will return true
        */
        convertDateFormat: function (data = null) {

            if (data === null) { requiredDate = new Date() }
            else { requiredDate = new Date(data) }
            try {
                var dd = requiredDate.getDate();
                if (dd < 10) {
                    dd = '0' + dd;
                }
                var mm = requiredDate.getMonth() + 1; //January is 0!
                if (mm < 10) {
                    mm = '0' + mm;
                }
                var yyyy = requiredDate.getFullYear();

                var hh = requiredDate.getHours();
                if (hh < 10) {
                    hh = '0' + hh;
                }
                var ii = requiredDate.getMinutes();
                if (ii < 10) {
                    ii = '0' + ii;
                }
                var ss = requiredDate.getSeconds();
                if (ss < 10) {
                    ss = '0' + ss;
                }

                return {
                    "status": true,
                    "pt": dd + '-' + mm + '-' + yyyy + ' ' + hh + ':' + ii + ':' + ss,
                    "mysql": yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + ii + ':' + ss,
                    "iso": requiredDate.toISOString()
                }
            } catch (error) {
                return {
                    "status": false,
                    "message": error.message
                }
            }
        },

        /**
         * turns YYYY-mm-dd HH:ii:ss to dd-mm-YYYY HH:ii:ss 
         * */
        formatDateEurope: function (string = null) {

            if (string === null) {
                return;
            }
            var time = "";
            if (string.length == 19) {
                var hour = string.substr(11, 2);
                hour = parseInt(hour);
                if (hour < 0 || hour > 23) { return ''; }
                var minut = string.substr(14, 2);
                minut = parseInt(minut);
                if (minut < 0 || minut > 59) { return ''; }
                var second = string.substr(17, 2);
                second = parseInt(second);
                if (second < 0 || second > 59) { return ''; }
                time = string.substr(10, 19);
            }
            if (string.length > 10) {
                string = string.substr(0, 10);
            }
            if (string.length == 10) {
                var a = string.substring(0, 4);
                var b = parseFloat(a);
                if (a * 1 != b * 1) { return ''; }
                var bits = string.split('-');
                var d = new Date(bits[0], bits[1] - 1, bits[2]);
                if (d && (d.getMonth() + 1) == bits[1] && d.getDate() == Number(bits[2])) {
                    return bits[2] + "-" + bits[1] + "-" + bits[0] + " " + time;
                }
            }
            return '';
        },

        isValidDate: function (data) {
            return this.convertDateFormat(data).status;
        },

        getNow: function (format = "") {
            if (format === "") {
                format = "mysql"
            }
            return this.convertDateFormat()[format];
        },

    },

    validateEmail: function(email){
        var mailformat = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if(email.match(mailformat)) {
            return true;
        } else {
            return false;
        }
    },

    numbersOnly: function (inputElement){
        var a=inputElement.value;
        var b=parseFloat(a);
        if (a*1 != b*1)	{inputElement.value= b;}
        
        if ( isNaN(b)){inputElement.value='';}	
    },

    isJson: function(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    },

}
