<?php

session_start();
	
include_once "../../vendor/autoload.php";


// declares the application settings
Configurations::setEnvParams();
Configurations::setGlobalConfigurations();

// implements the routing
Routing::route();

?>	