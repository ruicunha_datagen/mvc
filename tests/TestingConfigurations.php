<?php

class TestingConfigurations
{

	static function setGlobalConfigurations()	{

		error_reporting(E_ALL);
		ini_set('display_errors', 1);

		ini_set('memory_limit', '512M');
		if (empty($_SERVER['SERVER_NAME'])) {
			$_SERVER['SERVER_NAME'] = gethostname();
		}
		if (empty($_SERVER['SERVER_ADDR'])) {
			$_SERVER['SERVER_ADDR'] = gethostbyname($_SERVER['SERVER_NAME']);
		}
		define('PATH_API', "");

		define('MODELS_PATH', PATH_API . 'app/models/');
		define('CONTROLLERS_PATH', PATH_API . 'app/controllers/');
		define('VIEWS_PATH', PATH_API . 'app/views/');

		// mariaDB
		define('MYSQL_RO_CONNECTION_PARAMS', 	[
			"username" => "db_username_ro",
			"password" => "db_password_ro",
			"host" => "localhost",
			"database" => "db_name",
			"charset" => "utf8mb4"
		]);
		define('MYSQL_RW_CONNECTION_PARAMS', 	[
			"username" => "db_username_rw",
			"password" => "db_password_rw",
			"host" => "localhost",
			"database" => "db_name",
			"charset" => "utf8mb4"
		]);

	}

}
