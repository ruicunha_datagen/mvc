<?php

use PHPUnit\Framework\TestCase;

class RoutingTest extends TestCase
{

    protected function setUp(): void    {
        $_SESSION = [];
        $_POST=[];
        $_GET=[];
    }

    protected function tearDown(): void    {
        $_SESSION = [];
        $_POST=[];
        $_GET=[];
    }

    /**
     * Test ID: 1710434661
     * Testing default route
     * */
    public function test_route_default () {
        $return = Routing::route();
        $this->assertTrue($_GET['url']==="home/start", "1714747085 - When missing GET['url'] should be redirecting to the landing page.");
        self::checkConstants( "home", "start");
    }

    /**
     * Test ID: 1710434661
     * Testing response type enforcement
     * */
    public function test_response_type_enforcement () {
        $_GET['url']="Mock/test_controller_not_returning_response_type";
        $return = Routing::route();
        self::checkConstants( "Mock", "test_controller_not_returning_response_type");        
        $testOk = ( intval($return) === 1714748530 );
        $this->assertTrue( $testOk , "1714763512 - Routing is not enforcing a response type | The tested method should not have a response type.");
    }

    /**
     * Test ID: 1710434661
     * Testing dinamic routes | Non existing method and controller
     * */
    public function test_controller_is_enforced () {
        $_GET['url']="someController/someMethod";
        $return = Routing::route();        
        self::checkConstants( "someController", "someMethod");        
        $testOk = ( intval($return) === 1714748532 );
        $this->assertTrue( $testOk , "1714763513 - Routing is not enforcing an existing controller | The tested controller should not exist.");
    }

    /**
     * Test ID: 1710434661
     * Testing dinamic routes | Existing controller and non existing method
     * */
    public function test_method_is_enforced () {
        $_GET['url']="Mock/someMethod";
        $return = Routing::route();        
        self::checkConstants( "Mock", "someMethod");
        $testOk = ( intval($return) === 1714748533 );
        $this->assertTrue( $testOk , "1714763514 - Routing is not enforcing an existing method | The tested method should not exist.");
    }

    private function checkConstants( String $controller, String $method){
        $constant_CONTROLLER_NAME_isSet = ( defined("CONTROLLER_NAME") && CONTROLLER_NAME===$controller);        
        $this->assertTrue( $constant_CONTROLLER_NAME_isSet , "1714747089 - Constant CONTROLLER_NAME is not properly set.");
        $constant_METHOD_NAME_isSet = ( defined("METHOD_NAME") && METHOD_NAME===$method);
        $this->assertTrue( $constant_METHOD_NAME_isSet, "1714747090 - Constant METHOD_NAME is not properly set.");
    }


}
